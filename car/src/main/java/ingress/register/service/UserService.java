package ingress.car.service;


import ingress.car.dto.CreateDto;
import ingress.car.dto.UserDto;
import ingress.car.entity.User;
import ingress.car.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Optional;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public UserService(UserRepository userRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }

    public User login(UserDto userDto) {
     return null;
     // :(
    }


    public void register(CreateDto createDto) {
        User user = modelMapper.map(createDto, User.class);
        if (!createDto.getPassword().equals(createDto.getPassword2())){
            System.out.println("Passwords must be same");
        }
        userRepository.save(user);
    }
}
