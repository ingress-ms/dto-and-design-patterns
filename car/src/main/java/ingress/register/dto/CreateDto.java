package ingress.car.dto;

import lombok.Data;

@Data
public class CreateDto {

    private String name;
    private String email;
    private String password;
    private String password2;
}