package org.example;

public class Factory {
    public Playable getInstance(String instrument) {
        if (instrument.equals("Guitar")) {
            return new Guitar();
        } else if (instrument.equals("Piano")){
            return new Piano();
        } else return null;
    }
}
